REPO=https://github.com/Riccoo/kadai-tasklist

docker-compose down
rm -rf var/mysql
rm -rf app

git clone ${REPO} app
cd app && composer install
cd ..

cp .env app/.env

docker-compose build
docker-compose up -d
sleep 20
docker exec -it php php artisan migrate

echo "Ready: http://localhost"
